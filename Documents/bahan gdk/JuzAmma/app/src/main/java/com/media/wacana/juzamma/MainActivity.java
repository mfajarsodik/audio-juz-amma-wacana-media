package com.media.wacana.juzamma;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

public class MainActivity extends AppCompatActivity implements Runnable{

    private Button scanbtn;
    private ImageView toggle;
    private SeekBar soundSeekBar;
    private MediaPlayer soundPlayer;
    private Thread soundThread;
    private TextView judul;
    private LinearLayout audioLayout;

    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scanbtn = (Button) findViewById(R.id.scanbtn);
        toggle = (ImageView) findViewById(R.id.toggle);
        soundSeekBar = (SeekBar)findViewById(R.id.soundSeekBar);
        judul = (TextView) findViewById(R.id.audioName);
        audioLayout = (LinearLayout) findViewById(R.id.audioLayout);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    PERMISSION_REQUEST);
        }

        scanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        setupListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            if (data != null){
                final Barcode barcode = data.getParcelableExtra("barcode");
                final String audioFile = barcode.displayValue;

                switch (audioFile){
                    case "Al_Fatihah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_fatihah);
                        judul.setText("Al Fatihah");
                        audioInit();
                        break;
                    case "An_Naas":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.an_naas);
                        judul.setText("An Naas");
                        audioInit();
                        break;
                    case "Al_Falaq":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_falaq);
                        judul.setText("Al Falaq");
                        audioInit();
                        break;
                    case "Al_Ikhlas":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_ikhlas);
                        judul.setText("Al Ikhlas");
                        audioInit();
                        break;
                    case "Al_Lahab":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_lahab);
                        judul.setText("Al Lahab");
                        audioInit();
                        break;
                    case "An_Nasr":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.an_nasr);
                        judul.setText("An Nasr");
                        audioInit();
                        break;
                    case "Al_Kafirun":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_kafirun);
                        judul.setText("Al Kafirun");
                        audioInit();
                        break;
                    case "Al_Kautsar":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_kautsar);
                        judul.setText("Al Kautsar");
                        audioInit();
                        break;
                    case "Al_Maun":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_maun);
                        judul.setText("Al Maun");
                        audioInit();
                        break;
                    case "Al_Quraisy":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_quraisy);
                        judul.setText("Al Quraisy");
                        audioInit();
                        break;
                    case "Al_Fiil":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_fiil);
                        judul.setText("Al Fiil");
                        audioInit();
                        break;
                    case "Al_Humazah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_humazah);
                        judul.setText("Al Humazah");
                        audioInit();
                        break;
                    case "Al_Asr":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_asr);
                        judul.setText("Al Asr");
                        audioInit();
                        break;
                    case "At_Takatsur":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.at_takatsur);
                        judul.setText("At Takatsur");
                        audioInit();
                        break;
                    case "Al_Qariah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_qariah);
                        judul.setText("Al Qariah");
                        audioInit();
                        break;
                    case "Al_Adiyat":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_adiyat);
                        judul.setText("Al Adiyat");
                        audioInit();
                        break;
                    case "Al_Zalzalah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_zalzalah);
                        judul.setText("Al Zalzalah");
                        audioInit();
                        break;
                    case "Al_Bayyinah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_bayyinah);
                        judul.setText("Al Bayyinah");
                        audioInit();
                        break;
                    case "Al_Qadar":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_qadar);
                        judul.setText("Al Qadar");
                        audioInit();
                        break;
                    case "Al_Alaq":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_alaq);
                        judul.setText("Al Alaq");
                        audioInit();
                        break;
                    case "At_tin":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.at_tin);
                        judul.setText("At Tin");
                        audioInit();
                        break;
                    case "Al_Insyirah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_insyirah);
                        judul.setText("Al Insyirah");
                        audioInit();
                        break;
                    case "Adh_Dhuha":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.adh_dhuha);
                        judul.setText("Adh Dhuha");
                        audioInit();
                        break;
                    case "Al_Lail":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_lail);
                        judul.setText("Al Lail");
                        audioInit();
                        break;
                    case "Asy_Syams":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.asy_syams);
                        judul.setText("Asy Syams");
                        audioInit();
                        break;
                    case "Al_Balad":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_balad);
                        judul.setText("Al Balad");
                        audioInit();
                        break;
                    case "Al_Fajr":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_fajr);
                        judul.setText("Al Fajr");
                        audioInit();
                        break;
                    case "Al_Ghasiyah":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_ghasiyah);
                        judul.setText("Al Ghasiyah");
                        audioInit();
                        break;
                    case "Al_Ala":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_ala);
                        judul.setText("Al A'la");
                        audioInit();
                        break;
                    case "Ath_Thoriq":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.ath_thoriq);
                        judul.setText("Ath Thoriq");
                        audioInit();
                        break;
                    case "Al_Insyiqaq":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_insyiqaq);
                        judul.setText("Al Insyiqaq");
                        audioInit();
                        break;
                    case "Al_Muthaffifin":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_muthaffifin);
                        judul.setText("Al Muthaffifin");
                        audioInit();
                        break;
                    case "Al_Infithar":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_infithar);
                        judul.setText("Al Infithar");
                        audioInit();
                        break;
                    case "At_Takwir":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.at_takwir);
                        judul.setText("At Takwir");
                        audioInit();
                        break;
                    case "Abasa":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.abasa);
                        judul.setText("Abasa");
                        audioInit();
                        break;
                    case "An_Naziat":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.an_naziat);
                        judul.setText("An Naziat");
                        audioInit();
                        break;
                    case "An_Naba":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.an_naba);
                        judul.setText("An Naba");
                        audioInit();
                        break;

                    case "Al_Buruj":
                        soundPlayer = MediaPlayer.create(this.getBaseContext(), R.raw.al_buruj);
                        judul.setText("Al Buruj");
                        audioInit();
                        break;

                    default:
                        Toast.makeText(this, "Data di QRCode Tidak Sesuai!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void setupListener() {
        // Toggle Button Setting
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (soundPlayer.isPlaying()) {
                    soundPlayer.pause();
                    toggle.setImageResource(R.drawable.play);
                } else {
                    soundPlayer.start();
                    toggle.setImageResource(R.drawable.pause);
                }
            }
        });

        soundSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    soundPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void audioInit(){
        toggle.setImageResource(R.drawable.pause);
        soundPlayer.start();
        soundThread = new Thread(this);
        soundThread.start();
        soundPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                toggle.requestLayout();
                toggle.setImageResource(R.drawable.play);
            }
        });
        audioLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void run() {
        int currentPosition = 0;
        int soundTotal = soundPlayer.getDuration();
        soundSeekBar.setMax(soundTotal);

        while (soundPlayer != null && currentPosition < soundTotal) {
            try {
                Thread.sleep(300);
                currentPosition = soundPlayer.getCurrentPosition();
            } catch (InterruptedException soundException) {
                return;
            } catch (Exception otherException) {
                return;
            }

            soundSeekBar.setProgress(currentPosition);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (audioLayout.getVisibility() == View.VISIBLE) {
            soundPlayer.pause();
            toggle.setImageResource(R.drawable.play);
        }
    }
}